#########################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2024 Carlos Scotta <saludpublica@ingenieria.uner.edu.ar>
#    Copyright (C) 2024 Ana Roskopf <saludpublica@ingenieria.uner.edu.ar>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################

from trytond.model import fields, ModelView
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Equal
from trytond.exceptions import UserError

import re


class PatientPrescriptionOrder(metaclass=PoolMeta):
    'Medication recall reminder'
    __name__ = 'gnuhealth.prescription.order'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
            'med_reminder_button': {
            }
        })

    @classmethod
    @ModelView.button_action('health_reminders_fiuner.act_med_reminder_open')
    def med_reminder_button(cls, records):
        for record in records:
            contact = record.patient.name.contact_mechanisms
