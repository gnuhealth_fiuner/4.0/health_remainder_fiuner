from trytond.model import fields, ModelView
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Equal
from trytond.exceptions import UserError

import re


class Appointment(metaclass=PoolMeta):
    __name__ ='gnuhealth.appointment'

    message_sent = fields.Boolean('Message Sent')
    confirm_app = fields.Boolean('Answer',
            help='Tildar en caso de recibir respuesta '
            'sobre el turno por el paciente')
    release_app=fields.Boolean('Release App')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
            'reminder_button': {
                'invisible': ~Equal(Eval('state'),'confirmed')
            },
            'appcancel_button': {
                'invisible': ~Equal(Eval('state'),'confirmed')
            }
        })

    @classmethod
    @ModelView.button_action('health_reminders_fiuner.act_appointments_reminder_open')
    def reminder_button(self, records):
        for record in records:
            record.validate_number()

    @classmethod
    @ModelView.button_action('health_reminders_fiuner.act_appointments_cancel_open')
    def appcancel_button(self, records):
        for record in records:
            record.validate_number()

    def validate_number(self):
        contact = self.patient.name.contact_mechanisms
        if contact:
            for mechanism in contact:
                if mechanism.type == "phone": 
                    phone_number = mechanism.value
                    amount = r'\d{9}'
                    results = re.match(amount, phone_number)

                    if results is None:
                        raise UserError("Paciente con número de teléfono incorrecto")
        else:
            raise UserError("Paciente sin un número de teléfono asignado")
        pass
