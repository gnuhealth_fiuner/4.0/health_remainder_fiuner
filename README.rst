========================================================================

Reminders via messaging with GNU Health

========================================================================

Reminders through messaging with GNU Health is a tool developed by the Group of Studies in Public Health and Applied Technologies of the Faculty of Engineering of the National University of Entre Ríos (UNER), the "Humberto D'Angelo" Primary Health Care Center of the city of Paraná, Entre Ríos and the Ministry of Health of Entre Ríos (Argentina). Ana E. Roskopf also participated as part of her internship in the Bioengineering career at UNER.

Reminders through messaging with GNU Health allows sending reminders through the GNU Health system. It is interoperable with the Ministry of Health of Entre Ríos.

This version includes:
* Appointments
* Pharmacy
* Ambulatory Care
* Patients with chronic non-communicable diseases

Based on:
GNU Health (https://www.gnuhealth.org/)
Tryton (https://www.tryton.org/)

It is developed with the following languages
Python
HTML

****-------------------------------------------Implementation------------------------------------------****

The module is installed like any of the other GNU Health modules. The extra requirement is to use the url of the Ministry of Health, with your designated User and Password.

For more information on everything contained, please consult the documentation available (in Spanish) "Recordatorios por medio de mensajería con GNU Health.pdf" available in this development.

========================================================================

Reminders via messaging with GNU Health

========================================================================

Reminders through messaging with GNU Health is a tool developed by the Group of Studies in Public Health and Applied Technologies of the Faculty of Engineering of the National University of Entre Ríos (Argentina), the "Humberto D'Angelo" Primary Health Care Center of the city of Paraná, Entre Ríos and the Ministry of Health of Entre Ríos (Argentina).

This development allows professionals using GNU Health to send reminders to patients:

* Appointments
* Medication of chronic non-communicable diseases


Homepage
--------

http://ingenieria.uner.edu.ar/grupos/salud_publica/


Download
-------------

https://gitlab.com/gnuhealth_fiuner

Documentation
-------------

See the documentation on the GitLab repository


Support GNU Health Web Dashboard and Patients Caller
---------------------------------------------------
Please contact:

saludpublica@ingenieria.uner.edu.ar
carlos.scotta@uner.edu.ar


Need help to implement GNU Health Web Dashboard or the Patients Caller? 
----------------------------------------------------------------------

We will do our best to help you out with the implementation and training
for the local team, to build local capacity and make your project sustainable.

Please contact us and we'll back to you as soon as possible.


 Thank you !
 Bioing. Scotta Carlos José
 Author
 carlos.scotta@uner.edu.ar


Email
-----
saludpublica@ingenieria.uner.edu.ar
carlos.scotta@uner.edu.ar
ana.roskopf@uner.edu.ar


License
--------

GNU Health Web Dashboard and Patients Caller is licensed under GPL v3+::


 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
