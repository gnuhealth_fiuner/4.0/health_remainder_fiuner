from trytond.model import fields
from trytond.wizard import StateTransition
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval
from trytond.exceptions import UserError

from datetime import date
import locale
import requests
import re


class ReminderMessageTemplate():

    message = fields.Text('Mensaje', required=True)
    phone = fields.Char('Phone', required=True)
    phone_selection = fields.Many2One('party.contact_mechanism', "Phone",
            domain=[('id', 'in', Eval('phone_domain'))])
    phone_domain = fields.Many2Many('party.contact_mechanism',
            None, None, "Other phone domain")
    check_phone_format = fields.Text("Check phone format",
            states={'invisible': Eval('check_phone_format') == ''},
            readonly=True
            )

    # Chequeamos cuando cambia la selección del telefono
    @fields.depends('phone', 'check_phone_format', 'phone_selection')
    def on_change_phone_selection(self, name=None):
        if self.phone_selection:
            phone = self.phone_selection.value
            self.phone =phone
            amount = r'\d{10}'
            results = re.match(amount, self.phone)
            self.check_phone_format = ""
            if results is None:
                self.check_phone_format = ""
                +"Formato de número incorrecto. Debe tener 10 digitos"

    # Chequeamos cuando cambia el telefono
    @fields.depends('phone')
    def on_change_with_check_phone_format(self, name=None):
        if self.phone:
            amount = r'\d{10}'
            results = re.match(amount, self.phone)
            if results is None:
                return "Formato de número incorrecto. Debe tener 10 digitos"
        return ''

    @classmethod
    def default_message(self):
        defaults = "Something"
        return defaults


class ReminderTemplate():

    prevalidate = StateTransition()
    send = StateTransition()

    def generate_message(self, config_message, date, patient, healthprof, specialty, institution):
        pool = Pool()
        Config = pool.get('gnuhealth.reminder.config')
        config = Config(1)

        try:
            locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
            text_day = str(date.strftime('%A'))
            month = str(date.strftime('%B'))
        except:
            dow = config.dow.split()
            text_day = dow[date.weekday()]
            month_list = config.month.split()
            month = month_list[date.month - 1]

        date_aux = str(date.date())
        day = str(date.date().day)
        year = str(date.strftime('%Y'))
        patient_lastname = str(patient.name.lastname).upper()
        patient_name = str(patient.name.name)
        prof_lastname = str(healthprof.name.lastname).upper()
        prof_name = str(healthprof.name.name)
        specialty = specialty and str(specialty.name) or ''
        institution= institution and institution.rec_name or ''
        predef_message = config_message.replace(
            "{patient_name}", patient_name).replace(
            "{patient_lastname}", patient_lastname).replace(
            "{institution}", institution).replace(
            "{text_day}", text_day).replace(
            "{day}", day).replace(
            "{month}", month).replace(
            "{year}", year).replace(
            "{prof_lastname}", prof_lastname).replace(
            "{prof_name}", prof_name).replace(
            "{specialty}", specialty)
        return predef_message

    def get_response(self, phone, message):
        pool = Pool()
        ReminderConfig = pool.get('gnuhealth.reminder.config')

        config = ReminderConfig(1)
        transaction_id = Transaction().context.get('active_id')

        print(config.app_id, " << ", config.app_key )

        try:
            datos = {
                "user": config.app_id,
                "pass": config.app_key,
                "numerotelefono": phone,
                "mensaje": message
            }
            url = config.url
            response = requests.post(
                url, data=datos,
                headers={'Content-Type': 'application/x-www-form-urlencoded'})
            print("---- El mensaje se envió, todo: ",response.text)
            response = response.text
        except Exception as e:
            raise UserError(f"Se produjo un error al intentar enviar el mensaje: {e}")
        return response

    def put_ok_on_model():
        pass

    def transition_prevalidate(self):
        if self.start.check_phone_format:
            raise UserError("Formato de número incorrecto. Debe tener 10 digitos")
        return 'send'

    def transition_send(self):
        response = self.get_response(self.start.phone, self.start.message).strip()
        if response == 'OK':
            print("**************** Acción de enviar por Wsp ejecutada ****************")
            self.put_ok_on_model()
        return 'end'
