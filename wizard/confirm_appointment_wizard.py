#########################################################################
#                           reminder_wizard.py                          #
#########################################################################

from trytond.model import ModelView
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, Button
from trytond.transaction import Transaction
from trytond.exceptions import UserError

from .reminder_template import ReminderMessageTemplate, ReminderTemplate

from datetime import datetime

class AppointmentsReminderView(ModelView, ReminderMessageTemplate):
    'Creation of an appointment reminder view'
    __name__ = 'gnuhealth.appointment.reminder_wizard.start'


class AppointmentsReminderWizard(Wizard, ReminderTemplate):
    'Appointment Reminder Wizard'
    __name__ ='gnuhealth.appointment.reminder_wizard.wizard'

    start = StateView('gnuhealth.appointment.reminder_wizard.start',
        'health_reminders_fiuner.app_confirmation_reminder_wizard',[
            Button('Cancelar','end','tryton-cancel'),
            Button('Enviar por Wsp','prevalidate','tryton-ok',default=True),
            ])

    def default_start(self, records):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        Config = pool.get('gnuhealth.reminder.config')

        config = Config(1)
        transaction_id = Transaction().context.get('active_id')
        app = Appointment(transaction_id)
        config_message = config.appointment_reminder
        predef_message = self.generate_message(config_message, app.appointment_date,
                            app.patient, app.healthprof, app.speciality, app.institution)

        # Buscamos el telefono, el dominio y la lista
        phone = app.phone
        phone_domain = [p.id for p in app.patient.name.contact_mechanisms
                        if p.type == 'phone']
        phone_selection = phone_domain and phone_domain[0]
        return {
            'message': predef_message,
            'phone': phone,
            'phone_selection': phone_selection,
            'phone_domain': phone_domain
            }

    def put_ok_on_model(self):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        ReminderMessageLog = pool.get('gnuhealth.reminder.message_log')
        transaction_id = Transaction().context.get('active_id')

        app = Appointment(transaction_id)
        app.message_sent = True
        app.save()

        reminder = ReminderMessageLog()
        reminder.type_ = 'confirmation'
        reminder.origin = ','.join(['gnuhealth.appointment', str(app.id)])
        reminder.state = 'sent'
        reminder.phone_number = self.start.phone
        reminder.message_content = self.start.message
        reminder.date_sent = datetime.today()
        reminder.save()
