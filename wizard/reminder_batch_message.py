from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, StateAction, Button
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.modules.health.core import get_institution

from .reminder_template import ReminderMessageTemplate, ReminderTemplate

from datetime import datetime, time
import pytz


class ReminderBatchStart(ModelView):
    "Reminder Batch - Start"
    __name__ = 'gnuhealth.reminder.batch.start'

    message_type = fields.Selection([
        (None, ''),
        ('app_confirmation', "Appointment confirmation"),
        ('app_cancelation', "Appointment cancelation"),
        ('presc_confirmation', "Prescription confirmation")
        ], "Message type", sort=False, required=True)
    message_template = fields.Text("Message template")
    date = fields.Date("Date", required=True)
    message_list = fields.One2Many('gnuhealth.reminder.batch.message_list',
        None, "Message list")
    health_profs = fields.Many2Many('gnuhealth.healthprofessional',
            None, None, "Health Professionals",
            domain=[('id', 'in', Eval('health_profs_domain'))],
            required=True)
    health_profs_domain = fields.Many2Many('gnuhealth.healthprofessional',
            None, None, "Health Professionals domain")

    @fields.depends('message_type')
    def on_change_with_message_template(self, name=None):
        pool = Pool()
        Config = pool.get('gnuhealth.reminder.config')

        config = Config(1)
        if self.message_type == 'app_confirmation':
            return config.appointment_reminder
        elif self.message_type == 'app_cancelation':
            return config.appointment_cancel
        elif self.message_type == 'presc_confirmation':
            return config.prescription_reminder
        return None

    def generate_message(self, patient,
                institution, date, healthprof, specialty):
        pool = Pool()
        Config = pool.get('gnuhealth.reminder.config')
        config = Config(1)

        try:
            locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
            text_day = str(date.strftime('%A'))
            month = str(date.strftime('%B'))
        except:
            dow = config.dow.split()
            text_day = dow[date.weekday()]
            month_list = config.month.split()
            month = month_list[date.month - 1]

        date_aux = str(date.date())
        day = str(date.date().day)
        year = str(date.strftime('%Y'))
        patient_lastname = str(patient.name.lastname).upper()
        patient_name = str(patient.name.name)
        prof_lastname = str(healthprof.name.lastname).upper()
        prof_name = str(healthprof.name.name)
        specialty = specialty and str(specialty.name) or ''
        institution= institution and institution.rec_name or ''


        message = self.message_template.replace(
            "{patient_name}", patient_name).replace(
            "{patient_lastname}", patient_lastname).replace(
            "{institution}", institution).replace(
            "{text_day}", text_day).replace(
            "{day}", day).replace(
            "{month}", month).replace(
            "{year}", year).replace(
            "{prof_lastname}", prof_lastname).replace(
            "{prof_name}", prof_name).replace(
            "{specialty}", specialty)
        return message

    @fields.depends('date', 'message_type', 'message_template',
            'date', 'health_profs')
    def on_change_with_message_list(self, name=None):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        Prescription = pool.get('gnuhealth.prescription.order')
        Institution = pool.get('gnuhealth.institution')
        Company = pool.get('company.company')
        company_id = Transaction().context.get('company')
        company = Company(company_id)

        message_list = []

        if self.message_type and self.message_template and self.date and self.health_profs:
            max_date_utc = datetime.combine(self.date, datetime.max.time())
            max_date_utc = max_date_utc.replace(tzinfo=pytz.timezone(company.timezone))

            min_date_utc = datetime.combine(self.date, datetime.min.time())
            min_date_utc = min_date_utc.replace(tzinfo=pytz.timezone(company.timezone))

            max_date = max_date_utc.astimezone(pytz.timezone('UTC'))
            min_date = min_date_utc.astimezone(pytz.timezone('UTC'))
            if self.message_type in ['app_confirmation', 'app_cancelation']:
                appointments = Appointment.search([
                    ('appointment_date', '>', min_date),
                    ('appointment_date', '<', max_date),
                    ('state', 'in', ['confirmed']),
                    ('healthprof', 'in', [hp.id for hp in self.health_profs])
                    ])
                for app in appointments:
                    message =self.generate_message(
                        app.patient,
                        app.institution,
                        app.appointment_date,
                        app.healthprof,
                        app.speciality)
                    message_list.append({
                        'origin': str(app),
                        'message': message,
                        'phone': app.phone,
                        'phone_selection':
                            app.patient.name.contact_mechanisms and app.patient.name.contact_mechanisms[0].id,
                        'phone_domain': [x.id for x in app.patient.name.contact_mechanisms
                                if x.type in ['phone', 'mobile']]
                        })
            elif self.message_type == 'presc_confirmation':
                prescriptions = Prescription.search([
                    ('prescription_date', '>', min_date),
                    ('prescription_date', '<', max_date),
                    ('state', 'in', ['done']),
                    ('healthprof', 'in', [hp.id for hp in self.health_profs])
                    ])
                for presc in prescriptions:
                    message =self.generate_message(
                        presc.patient,
                        Institution(get_institution()),
                        presc.prescription_date,
                        presc.healthprof,
                        presc.healthprof.main_specialty)
                    message_list.append({
                        'origin': str(presc),
                        'message': message,
                        'phone': presc.patient.phone,
                        'phone_selection':
                            presc.patient.name.contact_mechanisms and presc.patient.name.contact_mechanisms[0].id,
                        'phone_domain': [x.id for x in presc.patient.name.contact_mechanisms
                                    if x.type in ['phone', 'mobile']]
                        })
        return message_list

    @fields.depends('date', 'message_type')
    def on_change_with_health_profs_domain(self, name=None):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        Prescription = pool.get('gnuhealth.prescription.order')
        Company = pool.get('company.company')
        company_id = Transaction().context.get('company')
        company = Company(company_id)

        if self.date and self.message_type:
            max_date_utc = datetime.combine(self.date, datetime.max.time())
            max_date_utc = max_date_utc.replace(tzinfo=pytz.timezone(company.timezone))

            min_date_utc = datetime.combine(self.date, datetime.min.time())
            min_date_utc = min_date_utc.replace(tzinfo=pytz.timezone(company.timezone))

            max_date = max_date_utc.astimezone(pytz.timezone('UTC'))
            min_date = min_date_utc.astimezone(pytz.timezone('UTC'))
            if self.message_type in ['app_confirmation', 'app_cancelation']:
                appointments = Appointment.search([
                    ('appointment_date', '>', min_date),
                    ('appointment_date', '<', max_date),
                    ('state', 'in', ['confirmed'])
                    ])
                return [x.healthprof.id for x in appointments if x.healthprof]
            elif self.message_type in ['presc_confirmation']:
                prescriptions = Prescription.search([
                    ('prescription_date', '>', min_date),
                    ('prescription_date', '<', max_date),
                    ('state', 'in', ['done'])
                    ])
                return [x.healthprof.id for x in prescriptions if x.healthprof]
        return []

    @fields.depends('date', 'message_type')
    def on_change_with_health_profs(self, name=None):
        return []

class ReminderBatchMessageList(ModelView, ReminderMessageTemplate):
    "Reminder Batch - Message List"
    __name__ = 'gnuhealth.reminder.batch.message_list'

    check_phone_format_icon = fields.Char("Check Phone Format Icon")
    origin = fields.Char("Origin")

    @fields.depends('phone')
    def on_change_with_phone_format_icon(self, name=None):
        return 'tryton-ok'


class ReminderBatchWizard(Wizard, ReminderTemplate):
    "Reminder Batch - Wizard"
    __name__ = 'gnuhealth.reminder.batch.wizard'

    start = StateView('gnuhealth.reminder.batch.start',
            'health_reminders_fiuner.gnuhealth_reminder_batch_start',[
                Button('Send', 'validate', 'tryton-ok', default=True),
                Button('Cancel', 'end', 'tryton-cancel')])

    validate = StateTransition()

    create_queue = StateAction('health_reminders_fiuner.reminder_message_log_action')

    def default_start(self, fields):
        return {}

    def transition_validate(self):

        return 'create_queue'

    def do_create_queue(self, action):
        pool = Pool()
        ReminderLog = pool.get('gnuhealth.reminder.message_log')
        logs = []

        for message in self.start.message_list:
            log = ReminderLog()
            log.origin = message.origin
            log.type_ = 'confirmation' if self.start.message_type == 'app_confirmation' \
                else 'cancelation' if message.message_type == 'app_cancelation' \
                else 'confirmation' if message.message_type == 'presc_confirmation' \
                else ''
            log.state = 'queue'
            log.phone_number = message.phone
            log.message_content =message.message
            log.save()
            logs.append(log)

        data = {'res_id': [log.id for log in logs]}

        return action, data
