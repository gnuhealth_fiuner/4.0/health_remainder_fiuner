#########################################################################
#                           reminder_wizard.py                          #
#########################################################################

from trytond.model import ModelView
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, Button
from trytond.transaction import Transaction
from trytond.exceptions import UserError

from .reminder_template import ReminderMessageTemplate, ReminderTemplate

from datetime import datetime


class MedicineReminder(ModelView, ReminderMessageTemplate):
    'Creation of an medicine reminder view'
    __name__ = 'gnuhealth.prescription.order.reminder_wizard.start'


class MedicineReminderWizard(Wizard, ReminderTemplate):
    'Medicine Reminder Wizard'
    __name__ ='gnuhealth.prescription.order.reminder_wizard.wizard'

    start = StateView('gnuhealth.prescription.order.reminder_wizard.start',
        'health_reminders_fiuner.prescription_confirmation_reminder_start', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Send via Wsp', 'send', 'tryton-ok'),
        ])

    def default_start(self, records):
        pool = Pool()
        Prescription = pool.get('gnuhealth.prescription.order')
        Config = pool.get('gnuhealth.reminder.config')
        ContactMechanism = pool.get('party.contact_mechanism')

        config = Config(1)
        transaction_id = Transaction().context.get('active_id')
        presc = Prescription(transaction_id)
        config_message = config.prescription_reminder
        institution = presc.healthprof.institution
        predef_message = self.generate_message(config_message, presc.prescription_date,
                            presc.patient, presc.healthprof, '', institution)

        # Buscamos el telefono, el dominio y la lista
        phone_domain = [p.id for p in presc.patient.name.contact_mechanisms
                        if p.type == 'phone']
        phone_selection = phone_domain and phone_domain[0]
        phone = phone_selection and ContactMechanism(phone_selection).value
        return {
            'message': predef_message,
            'phone': phone,
            'phone_selection': phone_selection,
            'phone_domain': phone_domain
            }

    def put_ok_on_model(self):
        pool = Pool()
        Prescription = pool.get('gnuhealth.prescription.order')
        ReminderMessageLog = pool.get('gnuhealth.reminder.message_log')
        transaction_id = Transaction().context.get('active_id')

        reminder = ReminderMessageLog()
        reminder.type_ = 'confirmation'
        reminder.origin = ','.join(['gnuhealth.prescription.order', str(transaction_id)])
        reminder.state = 'sent'
        reminder.phone_number = self.start.phone
        reminder.message_content = self.start.message
        reminder.date_sent = datetime.today()
        reminder.save()
