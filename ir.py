from trytond.pool import PoolMeta


class Cron(metaclass=PoolMeta):
    __name__ = 'ir.cron'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.method.selection.append(
            ('gnuhealth.reminder.message_log|cron_send_messages',
                "Send Whatsapp Queue Messages in Log"))
