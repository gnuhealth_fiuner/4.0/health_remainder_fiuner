#########################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2024 Carlos Scotta <saludpublica@ingenieria.uner.edu.ar>
#    Copyright (C) 2024 Ana Roskopf <saludpublica@ingenieria.uner.edu.ar>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################


from trytond.pool import Pool

from . import configuration
from . import reminder
from . import appointment
from . import prescription
from . import reminder
from . import ir

from .wizard import confirm_appointment_wizard
from .wizard import confirm_prescription_wizard
from .wizard import cancel_appointment_wizard
from .wizard import reminder_batch_message


def register():
    Pool.register(
        configuration.ReminderConfig,
        reminder.ReminderMessageLog,
        appointment.Appointment,
        prescription.PatientPrescriptionOrder,
        confirm_appointment_wizard.AppointmentsReminderView,
        confirm_prescription_wizard.MedicineReminder,
        cancel_appointment_wizard.CancelAppointmentsView,
        reminder_batch_message.ReminderBatchStart,
        reminder_batch_message.ReminderBatchMessageList,
        ir.Cron,
        module='health_reminders_fiuner', type_='model')
    Pool.register(
        confirm_appointment_wizard.AppointmentsReminderWizard,
        confirm_prescription_wizard.MedicineReminderWizard,
        cancel_appointment_wizard.CancelAppointmentsWizard,
        reminder_batch_message.ReminderBatchWizard,
        module='health_reminders_fiuner', type_='wizard')

