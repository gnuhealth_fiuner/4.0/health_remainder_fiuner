from trytond.model import fields, ModelView, ModelSQL
from trytond.pool import Pool
from trytond.pyson import Eval, Equal
from trytond.exceptions import UserError
from trytond.transaction import Transaction
from .wizard.reminder_template import ReminderTemplate

from datetime import datetime


class ReminderMessageLog(ModelView, ModelSQL, ReminderTemplate):
    "Reminder - Message Log"
    __name__ = 'gnuhealth.reminder.message_log'

    origin = fields.Reference("Origin", selection="get_origin", select=True,
                readonly=True)
    type_ = fields.Selection([
        (None, ''),
        ('cancelation', "Cancelation"),
        ('confirmation', "Confirmation")
        ], "Type", sort=False, readonly=True)
    patient = fields.Function(
            fields.Many2One('gnuhealth.patient', "Patient"),
            "on_change_with_patient")
    state = fields.Selection([
        (None, ''),
        ('draft', "Draft"),
        ('sent', "Sent"),
        ('queue', "Queue"),
        ('failed', "Failed"),
        ], "State", sort=False, readonly=True)
    phone_number = fields.Char('Phone number', readonly=True)
    message_content = fields.Text("Message content", readonly=True)
    date_scheduled = fields.DateTime("Date Scheduled", readonly=True)
    date_sent = fields.DateTime("Date sent", readonly=True)
    error_catch = fields.Char("Error catch", readonly=True)

    @fields.depends('origin')
    def on_change_with_patient(self,name=None):
        pool = Pool()
        if self.origin and int(str(self.origin).split(',')[1])>0:
            split = str(self.origin).split(',')
            model = split[0]
            id_ = int(split[1])
            record =pool.get(model)(id_)
            if model == 'gnuhealth.appointment':
                return record.patient.id
            elif model == 'gnuhealth.prescription.order':
                return record.patient.id
        return None


    @classmethod
    def get_origin(cls):
        return [
            ('gnuhealth.appointment', "Appointment"),
            ('gnuhealth.prescription.order', "Prescription")
            ]

    @property
    def origin_name(self):
        return self.origin.rec_name if self.origin else None


    @classmethod
    @ModelView.button
    def send(cls, logs):
        for log in logs:
            print(log, log.phone_number, log.origin)
            log.date_sent = datetime.today()
            log.error_catch = ''
            try:
                log.get_response(log.phone_number, log.message_content)
                log.state = 'sent'
                log.save()
            except Exception as e:
                log.error_catch = e
                log.save()

    @classmethod
    def cron_send_messages(cls):
        """Cron job to send messages in 'queue' state."""
        logs = cls.search([
            ('state', '=', 'queue')], limit=1)
        if logs:
            cls.send(logs)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
            'send': {
                'invisible': Equal(Eval('state'),'sent')
            },
        })
