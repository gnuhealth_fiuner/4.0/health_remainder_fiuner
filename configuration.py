from trytond.model import ModelSQL, ModelView, ModelSingleton, fields
from trytond.i18n import gettext


class ReminderConfig(ModelSingleton, ModelSQL, ModelView):
    'Reminder Configuration'
    __name__ = 'gnuhealth.reminder.config'

    url = fields.Char('Reminder Server URL', required=True,
        help='Base URL to access the  Server')
    app_id = fields.Char('App ID', help='ID to use in a production server')
    app_key = fields.Char('App Key', help='Key to use in a production server')

    appointment_reminder = fields.Text("Appointment remainder", required=True)
    appointment_cancel = fields.Text("Appointment cancel", required=True)
    prescription_reminder = fields.Text("Prescription remainder", required=True)
    references = fields.Text("References", readonly=True)
    dow = fields.Char("Days of the week", required=True)
    month = fields.Char("Months", required=True)

    @staticmethod
    def default_url():
        return 'https://geoloc.com.ar/turnera/ws'

    @staticmethod
    def default_appointment_reminder():
        return gettext('health_reminders_fiuner.msg_appointment_reminder').strip()

    @staticmethod
    def default_appointment_cancel():
        return gettext('health_reminders_fiuner.msg_appointment_cancel').strip()

    @staticmethod
    def default_prescription_reminder():
        return gettext('health_reminders_fiuner.msg_prescription_reminder').strip()

    @staticmethod
    def default_references():
        return gettext('health_reminders_fiuner.msg_references').strip()

    @staticmethod
    def default_dow():
        return gettext('health_reminders_fiuner.msg_dow')

    @staticmethod
    def default_month():
        return gettext('health_reminders_fiuner.msg_month')
